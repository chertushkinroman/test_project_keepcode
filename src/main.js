import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import CustomSelect from '@/components/CustomSelect.vue'



const app = createApp(App)
app.component('v-select', CustomSelect)
app.use(router)
app.mount('#app')
