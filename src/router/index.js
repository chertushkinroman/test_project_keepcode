import { createRouter, createWebHistory } from 'vue-router'

const routes = [
  {
    path: '/',
    name: 'Main',
    component: () => import('../pages/Main.vue')
  },
  {
    path: '/students',
    name: 'Students',
    component: () => import('../pages/Students/Students.vue')
  },
  {
    path: '/teachers',
    name: 'Teachers',
    component: () => import('../pages/Teachers.vue')
  },
  {
    path: '/lessons',
    name: 'Leson',
    component: () => import('../pages/Lesons.vue')
  },
  {
    path: '/employees',
    name: 'Employees',
    component: () => import('../pages/Employees.vue')
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
